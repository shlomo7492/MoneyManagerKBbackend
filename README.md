# MoneyManagerKBackend

local baseUrl http://localhost:4000

ROUTES:
-- Financial event--
GET: /finevent - Gets all income records for the currently logged user
GET: /finevent/startD/endD/type - Gets all income records for the currently logged user created between startD and endD  
 (dates format YYYY-MM-DD hh:mm:ii)
GET: /finevent/id - Checks if 'id' in ["expense","income"] if so it returns expenses or incomes, else it trets 'id' as financial event 'id' and gets financial event record with "id" for the currently logged user  
POST /finevent - create income record for the currently logged user
PATCH /finevent/id - updates income record with "id" for the currently logged user
DELETE /finevent/id deletes income record with "id" for the currently logged user

--USERS--
POST: /users - Creates new user with name, email, password
POST: /users/login - logs the user with correct credentials (email +password) and generate the JWT tokens both access and refresh (the second one is recorded in the DB)
DELETE: /users/logout - logs out the user, deletes the refresh token from the DB
