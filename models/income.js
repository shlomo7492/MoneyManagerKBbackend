const mongoose = require("mongoose");

const incomeSchema = new mongoose.Schema({
  amount: { type: Number, required: true },
  date: { type: Date, required: true, default: Date.now },
  description: { type: String, required: true },
  userId: { type: String, required: true }
});

module.exports = mongoose.model("Incomes", incomeSchema);
