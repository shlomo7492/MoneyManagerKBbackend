const mongoose = require("mongoose");

const financialEventSchema = new mongoose.Schema({
  type: { type: String, required: true },
  amount: { type: Number, required: true },
  date: { type: Date, required: true, default: Date.now },
  description: { type: String, required: true },
  category: { type: String, required: true },
  userId: { type: String, required: true }
});

module.exports = mongoose.model("FinancialEvent", financialEventSchema);
