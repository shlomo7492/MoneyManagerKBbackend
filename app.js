require("dotenv").config();
const express = require("express");
const app = express();
const mongoose = require("mongoose");

//DB connection
mongoose.connect(process.env.DATABASE_URL, {
  useUnifiedTopology: true,
  useNewUrlParser: true
});
const db = mongoose.connection;
db.on("error", error => console.error(error));
db.once("open", () => console.log("Connection to DB is successful"));

//Api server config
app.use(express.json());

// users/ end points
const usersRouter = require("./routes/users");
app.use("/users", usersRouter);

// expenses/ end points
const expensesRouter = require("./routes/expenses");
app.use("/expenses", expensesRouter);

// income/ end points
const incomeRouter = require("./routes/income");
app.use("/incomes", incomeRouter);

// finevent/ end points
const financialRouter = require("./routes/financialEvents");
app.use("/finevent", financialRouter);
// categories/ end points
const categoryRouter = require("./routes/categories");
app.use("/categories", categoryRouter);

app.listen(3001, () => console.log("Server is Started on PORT:3001"));
