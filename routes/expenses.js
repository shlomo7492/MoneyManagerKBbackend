const express = require("express");
require("dotenv").config();
const cors = require("cors");
const expensesRouter = express.Router();
const Expense = require("../models/expense");
const RefreshToken = require("../models/refreshToken");
const jwt = require("jsonwebtoken");

expensesRouter.use(express.json());
//enables cors

var whitelist = ["http://127.0.0.1:3000", "http://localhost:3000"];

const corsOptions = {
  origin: function(origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  }
};
expensesRouter.use(cors(corsOptions));
//GET All
expensesRouter.get("/", authenticateToken, async (req, res) => {
  console.log(res.statusCode);
  try {
    const expenses = (await Expense.find()).filter(
      exp => exp.userId === req.user.userId
    );

    res.json({ data: expenses, accessToken: req.newToken });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
//GET One
expensesRouter.get("/:id", getExpense, authenticateToken, (req, res) => {
  console.log(21, "getOne id:", req);
  if (res.expense.userId !== req.user.userId) return res.sendStatus(403);
  res.json({ data: res.expense, accessToken: req.newToken });
});
//GET All By start and end date
expensesRouter.get("/:startD/:endD", authenticateToken, async (req, res) => {
  var filteredExpenses = (await Expense.find()).filter(
    exp =>
      new Date(exp.date) >= new Date(req.params.startD) &&
      new Date(exp.date) <= new Date(req.params.endD)
  );
  try {
    res.json({ data: filteredExpenses, accessToken: req.newToken });
  } catch (err) {
    res.sendStatus(500);
  }
});
//CREATE One
expensesRouter.post("/", authenticateToken, async (req, res) => {
  const expense = new Expense({
    amount: req.body.amount,
    description: req.body.description,
    userId: req.user.userId
  });
  try {
    const newExpense = await expense.save();
    res.status(201).json({
      data: newExpense,
      accessToken: req.newToken,
      cookies: typeof req.cookie
    });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

//UPDATE One
expensesRouter.patch(
  "/:id",
  authenticateToken,
  getExpense,
  async (req, res) => {
    if (res.expense.userId !== req.user.userId) return res.sendStatus(403);
    if (req.body.amount && req.body.amount !== null) {
      res.expense.amount = req.body.amount;
    }
    if (req.body.description && req.body.description !== null) {
      res.expense.description = req.body.description;
    }
    try {
      const updatedExpense = await res.expense.save();
      res.json({ data: updatedExpense, accessToken: req.newToken });
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  }
);
//DELETE One
expensesRouter.delete(
  "/:id",
  authenticateToken,
  getExpense,
  async (req, res) => {
    try {
      if (res.expense.userId !== req.user.userId) return res.sendStatus(403);
      await res.expense.remove();
      res.json({ message: "Expense deleted.", accessToken: req.newToken });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
);

async function getExpense(req, res, next) {
  let expense;
  try {
    expense = await Expense.findById(req.params.id);
    if (expense === null) {
      return res.status(404).json({ message: "We can't find this expense" });
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
  res.expense = expense;
  next();
}
async function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const accessT = authHeader && authHeader.split(" ")[1];
  const refreshT = authHeader && authHeader.split(" ")[2];
  if (accessT === null) return res.sendStatus(401);
  jwt.verify(accessT, process.env.ACCESS_TOKEN_SECRET, async (err, user) => {
    if (err) {
      const tokenRefresh = (await RefreshToken.find()).filter(
        token => token.token === refreshT
      );
      if (typeof tokenRefresh[0] === "undefined") {
        return res.sendStatus(401);
      } else {
        jwt.verify(refreshT, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
          if (err) {
            tokenRefresh[0].remove();
            return res.sendStatus(401);
          }
          console.log(143, user);
          req.user = user;
          req.newToken =
            jwt.sign(
              { name: user.name, userId: user.userId },
              process.env.ACCESS_TOKEN_SECRET,
              { expiresIn: "60s" }
            ) +
            " " +
            refreshT;
        });
      }
    } else {
      req.user = user;
      req.newToken = accessT + " " + refreshT;
    }
    next();
  });
}
module.exports = expensesRouter;
