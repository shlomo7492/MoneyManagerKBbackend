const express = require("express");
const incomeRouter = express.Router();
const cors = require("cors");
const Income = require("../models/income");
const RefreshToken = require("../models/refreshToken");
const jwt = require("jsonwebtoken");

incomeRouter.use(express.json());
//enables cors

var whitelist = ["http://127.0.0.1:3000", "http://localhost:3000"];

const corsOptions = {
  origin: function(origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  }
};
incomeRouter.use(cors(corsOptions));
//GET All
incomeRouter.get("/", authenticateToken, async (req, res) => {
  try {
    const incomes = (await Income.find()).filter(
      inc => inc.userId === req.user.userId
    );
    res.json({ data: incomes, accessToken: req.newToken });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

//GET One
incomeRouter.get("/:id", authenticateToken, getIncome, (req, res) => {
  if (res.income.userId !== req.user.userId) return res.sendStatus(403);
  res.json({ data: res.income, accessToken: req.newToken });
});
//GET All By start and end date
incomeRouter.get(
  "/:startD/:endD",
  //getIncome,
  authenticateToken,
  async (req, res) => {
    var filteredIncome = (await Income.find()).filter(
      inc =>
        new Date(inc.date) >= new Date(req.params.startD) &&
        new Date(inc.date) <= new Date(req.params.endD)
    );
    try {
      res.json({ data: filteredIncome, accessToken: req.newToken });
    } catch (err) {
      res.sendStatus(500);
    }
  }
);
//CREATE One
incomeRouter.post("/", authenticateToken, async (req, res) => {
  const income = new Income({
    amount: req.body.amount,
    description: req.body.description,
    userId: req.user.userId
  });
  try {
    const newIncome = await income.save();
    res.status(201).json({ data: newIncome, accessToken: req.newToken });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

//UPDATE One
incomeRouter.patch("/:id", authenticateToken, getIncome, async (req, res) => {
  if (res.income.userId !== req.user.userId) return res.sendStatus(403);
  if (req.body.amount && req.body.amount !== null) {
    res.income.amount = req.body.amount;
  }
  if (req.body.description && req.body.description !== null) {
    res.income.description = req.body.description;
  }
  try {
    const updatedIncome = await res.income.save();
    res.json({ data: updatedIncome, accessToken: req.newToken });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

//DELETE One
incomeRouter.delete("/:id", authenticateToken, getIncome, async (req, res) => {
  try {
    if (res.income.userId !== req.user.userId) return res.sendStatus(403);
    await res.income.remove();
    res.json({ message: "Income deleted.", accessToken: req.newToken });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

async function getIncome(req, res, next) {
  let income;
  try {
    income = await Income.findById(req.params.id);
    if (income === null) {
      return res.status(404).json({ message: "We can't find this income" });
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
  res.income = income;
  next();
}
async function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const accessT = authHeader && authHeader.split(" ")[1];
  const refreshT = authHeader && authHeader.split(" ")[2];
  if (accessT === null) return res.sendStatus(401);
  jwt.verify(accessT, process.env.ACCESS_TOKEN_SECRET, async (err, user) => {
    if (err) {
      const tokenRefresh = (await RefreshToken.find()).filter(
        token => token.token === refreshT
      );
      if (typeof tokenRefresh[0] === "undefined") {
        return res.sendStatus(401);
      } else {
        jwt.verify(refreshT, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
          if (err) {
            tokenRefresh[0].remove();
            return res.sendStatus(401);
          }
          req.user = user;
          req.newToken =
            jwt.sign(
              { name: user.name, userId: user.userId },
              process.env.ACCESS_TOKEN_SECRET,
              { expiresIn: "60s" }
            ) +
            " " +
            refreshT;
        });
      }
    } else {
      req.user = user;
      req.newToken = accessT + " " + refreshT;
    }
    next();
  });
}
module.exports = incomeRouter;
