const express = require("express");
require("dotenv").config();
const cors = require("cors");
const financialRouter = express.Router();
const FinancialEvent = require("../models/financialEvent");
const RefreshToken = require("../models/refreshToken");
const jwt = require("jsonwebtoken");

financialRouter.use(express.json());
//enables cors

var whitelist = ["http://127.0.0.1:3000", "http://localhost:3000"];

const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
  allowedHeaders: [
    "Origin",
    "Content-Type",
    "X-Requested-With",
    "Authorization",
    "Cookie",
    "Accept",
  ],
};
financialRouter.use(cors(corsOptions));
//GET All
financialRouter.get("/", authenticateToken, async (req, res) => {
  console.log(26, res.statusCode, res.user);
  console.log("5e0305dc3dffe24c5c32edeb" === req.user.userId);

  let filteredFinEvents = [];
  if (!req.query) {
    filteredFinEvents = (await FinancialEvent.find()).filter(
      (finE) => finE.userId === req.user.userId
    );
  } else if (req.query.category !== "all") {
    console.log(req.user);
    filteredFinEvents = (await FinancialEvent.find()).filter(
      (finE) =>
        new Date(finE.date) >= new Date(req.query.start) &&
        new Date(finE.date) <= new Date(req.query.end) &&
        finE.type === req.query.category &&
        finE.userId === req.user.userId
    );
  } else {
    filteredFinEvents = (await FinancialEvent.find()).filter(
      (finE) =>
        new Date(finE.date) >= new Date(req.query.start) &&
        new Date(finE.date) <= new Date(req.query.end) &&
        finE.userId === req.user.userId
    );
  }
  try {
    jwt.verify(
      req.newToken,
      process.env.ACCESS_TOKEN_SECRET,
      async (err, user) => {
        if (err) {
          return res.sendStatus(401);
        }
        console.log(56, req.newToken, user);
      }
    );
    res.json({ data: filteredFinEvents, accessToken: req.newToken });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
//GET One
financialRouter.get("/:id", getFinEvent, authenticateToken, (req, res) => {
  console.log(21, "getOne id:", req.params.id, res.finEvent, req.user);
  if (res.finEvent.userId !== req.user.userId) return res.sendStatus(403);
  res.json({ data: [res.finEvent], accessToken: req.newToken });
});
//GET All By start and end date

//CREATE One
financialRouter.post("/", authenticateToken, async (req, res) => {
  const finEvent = new FinancialEvent({
    type: req.body.type,
    amount: req.body.amount,
    date: req.body.date ? req.body.date : new Date(),
    description: req.body.description,
    userId: req.user.userId,
    category: req.body.category,
  });
  try {
    console.log(75, req.body, finEvent);
    const newFinEvent = await finEvent.save();
    res.status(201).json({
      data: newFinEvent,
      accessToken: req.newToken,
      cookies: typeof req.cookie,
    });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

//UPDATE One
financialRouter.patch(
  "/:id",
  authenticateToken,
  getFinEvent,
  async (req, res) => {
    console.log(93, res.finEvent);
    if (res.finEvent.userId !== req.user.userId) return res.sendStatus(403);
    if (req.body.amount && req.body.amount !== null) {
      res.finEvent.amount = req.body.amount;
    }
    if (req.body.description && req.body.description !== null) {
      res.finEvent.description = req.body.description;
    }
    if (req.body.type && req.body.type !== null) {
      res.finEvent.type = req.body.type;
    }
    if (req.body.date && req.body.date !== null) {
      res.finEvent.date = req.body.date;
    }
    if (req.body.category && req.body.category !== null) {
      res.finEvent.category = req.body.category;
    }
    console.log(107, res.finEvent);

    try {
      const updatedFinEvent = await res.finEvent.save();
      console.log(111, res.finEvent, updatedFinEvent);
      res.json({ data: updatedFinEvent, accessToken: req.newToken });
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  }
);
//DELETE One
financialRouter.delete(
  "/:id",
  authenticateToken,
  getFinEvent,
  async (req, res) => {
    try {
      if (res.finEvent.userId !== req.user.userId) return res.sendStatus(403);
      await res.finEvent.remove();
      res.json({ message: "Expense deleted.", accessToken: req.newToken });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
);

async function getFinEvent(req, res, next) {
  const type = ["expense", "income"];
  let finEvent;
  console.log(134, req.params.id);
  try {
    if (req.params.id) {
      finEvent = await FinancialEvent.findById(req.params.id);
      if (finEvent === null) {
        console.log(132, req.params.id);
        return res
          .status(404)
          .json({ message: "We can't find this financial event" });
      }
      console.log(144, finEvent);
      res.finEvent = finEvent;
      console.log(146, res.finEvent);
    } //else {
    //   finEvent = (await FinancialEvent.find()).filter(
    //     finE => finE.type === req.body.type
    //   );
    //   console.log(typeof finEvent);
    //   if (finEvent.length === 0) {
    //     return res.status(404).json({
    //       message: `We can't find any financial events of type: ${req.params.id}`
    //     });
    //   }
    res.finEvent = finEvent;
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
  next();
}
async function authenticateToken(req, res, next) {
  console.log(161, req.body, res.finEvent, req.params.id);
  const authHeader = req.headers["authorization"];
  const accessT = authHeader && authHeader.split(" ")[1];
  console.log(authHeader);
  if (accessT === null) return res.sendStatus(401);
  jwt.verify(accessT, process.env.ACCESS_TOKEN_SECRET, async (err, user) => {
    if (err) {
      return res.sendStatus(401);
    }
    req.user = user;
    console.log(181, accessT, user, req.user);
    req.newToken = jwt.sign(
      { name: user.name, userId: user.userId, userEmail: user.userEmail },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: process.env.TOKEN_EXPIRE }
    );
    console.log(187, user);
  });

  next();
}
module.exports = financialRouter;
