const express = require("express");
require("dotenv").config();
const cors = require("cors");
const categoryRouter = express.Router();
const Category = require("../models/category");
const RefreshToken = require("../models/refreshToken");
const jwt = require("jsonwebtoken");

categoryRouter.use(express.json());
//enables cors
const initialCategories = [
  {
    type: "income",
    name: "Salary",
    userId: "default",
    icon: "faMoneyCheckAlt",
  },
  {
    type: "income",
    name: "Petty cash",
    userId: "default",
    icon: "faMoneyBillWave",
  },
  { type: "income", name: "Bonus", userId: "default", icon: "faGift" },
  {
    type: "income",
    name: "Allowance",
    userId: "default",
    icon: "faFunnelDollar",
  },
  {
    type: "income",
    name: "Other",
    userId: "default",
    icon: "faHandHoldingUsd",
  },
  { type: "expense", name: "Food", userId: "default", icon: "faHamburger" },
  { type: "expense", name: "Clothing", userId: "default", icon: "faTshirt" },
  { type: "expense", name: "social life", userId: "default", icon: "faUsers" },
  {
    type: "expense",
    name: "Self development",
    userId: "default",
    icon: "faBookReader",
  },
  {
    type: "expense",
    name: "Transportation",
    userId: "default",
    icon: "faBusAlt",
  },
  {
    type: "expense",
    name: "Culture",
    userId: "default",
    icon: "faTheaterMasks",
  },
  { type: "expense", name: "Household", userId: "default", icon: "faHome" },
  { type: "expense", name: "Holiday", userId: "default", icon: "faSnowman" },
  {
    type: "expense",
    name: "Anniversary",
    userId: "default",
    icon: "faBirthdayCake",
  },
  { type: "expense", name: "Health", userId: "default", icon: "faMedkit" },
  {
    type: "expense",
    name: "Education",
    userId: "default",
    icon: "faUniversity",
  },
  { type: "expense", name: "Sport", userId: "default", icon: "faTableTennis" },
  { type: "expense", name: "Gift", userId: "default", icon: "faGifts" },
  { type: "expense", name: "Other", userId: "default", icon: "faReceipt" },
];
var whitelist = ["http://127.0.0.1:3000", "http://localhost:3000"];

const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
  credentials: true,
  allowedHeaders: [
    "Origin",
    "Content-Type",
    "X-Requested-With",
    "Authorization",
    "Cookie",
    "Accept",
  ],
};
categoryRouter.use(cors());
//GET All
categoryRouter.get("/", initDB, authenticateToken, async (req, res) => {
  console.log(91, res.statusCode);
  let categories;
  try {
    if (req.query.type !== "all") {
      categories = (await Category.find()).filter(
        (cat) =>
          (cat.userId === req.user.userId || cat.userId === "default") &&
          cat.type === req.query.type
      );
    } else {
      categories = (await Category.find()).filter(
        (cat) => cat.userId === req.user.userId || cat.userId === "default"
      );
    }

    res.json({ data: categories, accessToken: req.newToken });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
//GET One
categoryRouter.get("/:id", getCategory, authenticateToken, (req, res) => {
  console.log(21, "getOne id:", req);
  if (res.category.userId !== req.user.userId) return res.sendStatus(403);
  res.json({ data: res.category, accessToken: req.newToken });
});

//CREATE One
categoryRouter.post("/", authenticateToken, async (req, res) => {
  const category = new Category({
    type: req.body.type,
    name: req.body.name,
    userId: req.user.userId,
    icon: req.body.icon,
  });
  try {
    const newCategory = await category.save();
    res.status(201).json({
      data: newCategory,
      accessToken: req.newToken,
      cookies: typeof req.cookie,
    });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

//UPDATE One
categoryRouter.patch(
  "/:id",
  authenticateToken,
  getCategory,
  async (req, res) => {
    if (res.category.userId !== req.user.userId) return res.sendStatus(403);
    if (req.body.type && req.body.type !== null) {
      res.category.type = req.body.type;
    }
    if (req.body.name && req.body.name !== null) {
      res.category.name = req.body.name;
    }
    if (req.body.icon && req.body.icon !== null) {
      res.category.icon = req.body.icon;
    }
    try {
      const updatedCategory = await res.category.save();
      res.json({ data: updatedCategory, accessToken: req.newToken });
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  }
);
//DELETE One
categoryRouter.delete(
  "/:id",
  authenticateToken,
  getCategory,
  async (req, res) => {
    try {
      if (res.category.userId !== req.user.userId) return res.sendStatus(403);
      await res.category.remove();
      res.json({ message: "Category deleted.", accessToken: req.newToken });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
);

async function getCategory(req, res, next) {
  let category;
  try {
    category = await Category.findById(req.params.id);
    if (category === null) {
      return res
        .status(404)
        .json({ message: "We can't find this financial event" });
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
  res.category = category;
  next();
}
async function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const accessT = authHeader && authHeader.split(" ")[1];
  if (accessT === null) return res.sendStatus(401);
  jwt.verify(accessT, process.env.ACCESS_TOKEN_SECRET, async (err, user) => {
    if (err) {
      return res.sendStatus(401);
    }
    req.user = user;
    req.newToken = jwt.sign(
      { name: user.name, userId: user.userId, userEmail: user.userEmail },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: process.env.TOKEN_EXPIRE }
    );
  });

  next();
}
async function initDB(req, res, next) {
  const categories = await Category.find();
  if (categories.length === 0) {
    console.log(153, categories.length);
    initialCategories.map((el) => {
      const category = new Category(el);
      category.save();
    });
  }
  next();
}
module.exports = categoryRouter;
