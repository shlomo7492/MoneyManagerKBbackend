const express = require("express");
require("dotenv").config();
const cors = require("cors");
const userRouter = express.Router();
const RefreshToken = require("../models/refreshToken");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/users");

userRouter.use(express.json());

//enables cors

var whitelist = ["http://127.0.0.1:3000", "http://localhost:3000"];

const corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
  credentials: true,
  allowedHeaders: [
    "Origin",
    "Content-Type",
    "X-Requested-With",
    "Authorization",
    "Cookie",
    "Accept",
  ],
};
userRouter.use(cors(corsOptions));

//Get user
userRouter.get("/", authenticateToken, async (req, res) => {
  try {
    console.log(36, req.user);
    res.status(200).json({
      data: {
        userId: req.user.userId,
        userName: req.user.name,
        userEmail: req.user.userEmail,
      },
      accessToken: req.newToken,
    });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

//Create user
userRouter.post("/", async (req, res) => {
  console.log(req.body);
  try {
    const hashedPass = await bcrypt.hash(req.body.password, 10);
    const user = new User({
      name: req.body.name,
      email: req.body.email,
      password: hashedPass,
    });
    const newUser = await user.save();
    res.status(201).json({ name: newUser.name, register: true });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

//Login user
userRouter.post("/login", getUser, async (req, res) => {
  //res.header("Access-Control-Allow-Credentials", "true");
  try {
    if (await bcrypt.compare(req.body.password, res.user.password)) {
      const accessToken = jwt.sign(
        { name: res.user.name, userId: res.user.id, userEmail: res.user.email },
        process.env.ACCESS_TOKEN_SECRET,
        { expiresIn: process.env.TOKEN_EXPIRE }
      );
      res.cookie("a", accessToken, {
        expires: new Date(Date.now() + 900000),
        path: "/",
        //sameSite: "none",
        //secure: true
        httpOnly: true,
      });
      res.status(200).json({
        data: {
          userId: res.user.id,
          userName: res.user.name,
          userEmail: res.user.email,
        },
        accessToken: accessToken,
      });
    } else {
      res.sendStatus(401);
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

//Update user
userRouter.patch("/:id", getUser, authenticateToken, async (req, res) => {
  //res.header("Access-Control-Allow-Credentials", "true");
  console.log(99, req.body.password, req.body.newPassword);
  if (await bcrypt.compare(req.body.password, res.user.password)) {
    res.user.password = await bcrypt.hash(req.body.newPassword, 10);
  }

  try {
    const updatedUser = await res.user.save();
    res.status(200).json({
      data: {
        userId: res.user.id,
        userName: res.user.name,
        userEmail: res.user.email,
      },
      accessToken: req.newToken,
    });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

//Get logged user from DB
async function getUser(req, res, next) {
  let loggedUser;
  console.log(122, req.body.email, req.body.password, req.body.newPassword);
  try {
    const users = await User.find();
    if (users !== null) {
      users.map((user) => {
        if (user.email === req.body.email) {
          loggedUser = user;
        }
      });
      if (!loggedUser) {
        return res.status(401).json({ message: "User not found." });
      }
    } else {
      return res.status(500);
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
  res.user = loggedUser;
  next();
}
async function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const accessT = authHeader && authHeader.split(" ")[1];
  if (accessT === null) return res.sendStatus(401);
  jwt.verify(accessT, process.env.ACCESS_TOKEN_SECRET, async (err, user) => {
    if (err) {
      return res.sendStatus(401);
    }
    console.log(154, user);
    req.user = user;
    req.newToken = jwt.sign(
      { name: user.name, userId: user.userId, userEmail: user.userEmail },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: process.env.TOKEN_EXPIRE }
    );
  });

  next();
}
module.exports = userRouter;
